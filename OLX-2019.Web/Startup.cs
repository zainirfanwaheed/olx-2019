﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OLX_2019.Web.Startup))]
namespace OLX_2019.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
