﻿using System;
using OLX_2019.entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OLX_2019.Database
{
    class abc: DbContext
    {
        public abc() : base("OLX-2019-Connection")
        {}

        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
