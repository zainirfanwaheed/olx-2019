namespace OLX_2019.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "User_ID", c => c.Int());
            AddColumn("dbo.Users", "Picture", c => c.String());
            CreateIndex("dbo.Products", "User_ID");
            AddForeignKey("dbo.Products", "User_ID", "dbo.Users", "ID");
            DropColumn("dbo.Products", "Pictures");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "Pictures", c => c.String());
            DropForeignKey("dbo.Products", "User_ID", "dbo.Users");
            DropIndex("dbo.Products", new[] { "User_ID" });
            DropColumn("dbo.Users", "Picture");
            DropColumn("dbo.Products", "User_ID");
        }
    }
}
