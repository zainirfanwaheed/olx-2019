namespace OLX_2019.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Phone_Number = c.Long(nullable: false),
                        Email = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Products", "Pictures", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "Pictures");
            DropTable("dbo.Users");
        }
    }
}
